import os
from flask import Blueprint, Flask, redirect, url_for, render_template, request
import requests
from geopy.distance import great_circle
from geopy import distance
from dotenv import load_dotenv


searchDistance = Blueprint("searchDistance",__name__,template_folder="templates")
load_dotenv()

API_KEY = os.getenv("API_KEY")


def switchLongLat(args):
    splitted = args.split()
    stringCoordinate = splitted[1] + ',' + splitted[0] #Reverse the position of long & lat
    Coordinate = eval(stringCoordinate) #Coordinate as a tuple
    return Coordinate


@searchDistance.route("/", methods=["POST", "GET"])
def home():
    if request.method == 'POST':
        location = request.form.get("nm", False)
        return redirect(url_for("searchDistance.results", location = location))
    else:
        return render_template("index.html")


@searchDistance.route("/results=<location>")
def results(location):
    reqMKAD =  requests.get(f'https://geocode-maps.yandex.ru/1.x/?apikey={API_KEY}&format=json&geocode=MKAD&results=1').json()
    req = requests.get(f'https://geocode-maps.yandex.ru/1.x/?apikey={API_KEY}&format=json&geocode={location}').json()

    #CenterCoordinate of MKAD
    centerCoordinate = switchLongLat(reqMKAD['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])

    #Lower Corner Coordinate of MKAD
    point1Coordinate = switchLongLat(reqMKAD['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['boundedBy']['Envelope']['lowerCorner'])

    # Upper Corner Coordinate of MKAD
    point2Coordinate = switchLongLat(reqMKAD['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['boundedBy']['Envelope']['upperCorner'])

    radius = great_circle(point1Coordinate, point2Coordinate).km

    total_raw_data = req['response']['GeoObjectCollection']['featureMember']
    data = []

    for i in range(len(total_raw_data)):
        pointTest = switchLongLat(total_raw_data[i]['GeoObject']['Point']['pos'])
        dis = distance.distance(centerCoordinate, pointTest).km
        data.append(total_raw_data[i]['GeoObject'])
        if dis <= radius: #if Spot still inside radius MKAD
            data.append({'distance':0})
        else:
            data.append({'distance':int(great_circle(centerCoordinate, pointTest).km)})
    if data:
        return render_template("result.html", data=data)


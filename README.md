# About
A simple web app to find distance between given location or address to MKAD (Moscow Ring Road)

## Tools
This web app using Flask of python and Yandex API

More on the API here: https://yandex.ru/dev/maps/geocoder/doc/desc/reference/precision.html

## Library
Additional Library can be found in requirements.txt

## How it Works
The approach of obtaining the distance is by using **GEOPY** library with **GREAT_CIRCLE** method with the input of the 
center coordinate of MKAD and the coordinate of the location from the input which both obtained from the API 

The API also provides the upperbound and the lowerbound of the given location in case the location consists of certain area such as MKAD

The application won't measure the distance if the coordinate from the input still within the radius of MKAD (distance = 0)


<img width="204" alt="Capture" src="https://user-images.githubusercontent.com/5984684/129874380-48fea0ef-d5f7-4894-8780-86353c2ccb9d.PNG">
<img width="325" alt="Capture2" src="https://user-images.githubusercontent.com/5984684/129874417-e3ea208b-2ec4-4ecd-8f09-1add865f796b.PNG">


## What to Improve
This is just a basic application with limited knowledge of GIS. A lot still can be improved with the method of measuring, 
the accuracy and the design of the web application overall 



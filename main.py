from flask import Flask
from SearchApp.searchDistance import searchDistance
from logging import FileHandler, WARNING


app = Flask(__name__)
app.register_blueprint(searchDistance, url_prefix="/")

file_handler = FileHandler('error.txt')
file_handler.setLevel(WARNING)
app.logger.addHandler(file_handler)


if __name__ == '__main__':
    app.run(debug=True)


